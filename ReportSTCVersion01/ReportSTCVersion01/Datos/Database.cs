﻿using ReportSTCVersion01.Service;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace ReportSTCVersion01.Modelo
{
    public class Database
    {
        private SQLiteConnection _connection;
        static readonly object locker = new object();

        public Database()
        {
            _connection = DependencyService.Get<ISQLite>().GetConnection();
            _connection.CreateTable<CatalogoEstacion>();
            _connection.CreateTable<CatalogoLinea>();
            _connection.CreateTable<RelLineaEstacion>();
            _connection.CreateTable<Reporte>();
            _connection.CreateTable<Roles>();
            _connection.CreateTable<SubtipoIncidencia>();
            _connection.CreateTable<TipoIncidencia>();
            _connection.CreateTable<Usuario>();
        }

        public List<CatalogoEstacion> GetEstaciones(string nombreE)
        {
            int estacion = (from t in _connection.Table<CatalogoLinea>()
                            where t.NombreLinea == nombreE
                            select t.IdLinea).First();

            var resutado = (from t in _connection.Table<RelLineaEstacion>()
                            where t.IdRelLinea == estacion
                            select t.IdRelEstacion).Distinct().ToList();

            List<CatalogoEstacion> lce = new List<CatalogoEstacion>();
            foreach (int res in resutado)
            {
                CatalogoEstacion ce = new CatalogoEstacion();
                ce.IdEstacion = res;
                ce.NombreEstacion = (from t in _connection.Table<CatalogoEstacion>()
                                     where t.IdEstacion == res
                                     select t.NombreEstacion).First();
                lce.Add(ce);
            }
            return lce;            
        }

        public int GetIdIncidencia(string incidencia)
        {
            return (from t in _connection.Table<TipoIncidencia>()
                    where t.NombreTipoIncidencia == incidencia
                    select t.IdTipoIncidencia).First();
        }

        public int GetIdLinea(string linea)
        {
            return (from t in _connection.Table<CatalogoLinea>()
                    where t.NombreLinea == linea
                    select t.IdLinea).First();
        }

        public int GetIdEstacion(string estacion)
        {
            return (from t in _connection.Table<CatalogoEstacion>()
                    where t.NombreEstacion == estacion
                    select t.IdEstacion).First();
        }

        public int GetIdLineaEstacion(int linea, int estacion)
        {
            return (from t in _connection.Table<RelLineaEstacion>()
                    where t.IdRelEstacion == estacion && t.IdRelLinea == linea
                    select t.IdLineaEstacion).First();
        }

        public List<SubtipoIncidencia> GetSubtipo(int idTipo)
        {
            return _connection.Table<SubtipoIncidencia>().Where(
                r=> r.IdRelTipoIncidencia == idTipo).ToList();
        }

        public void CargaEstaciones()
        {                     
            if (_connection.Table<CatalogoEstacion>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO CatalogoEstacion " +
                        "(IdEstacion,NombreEstacion) VALUES (1, 'OBSERVATORIO'), " +
                        "(2, 'TACUBAYA'), (3, 'JUANACATLAN'), (4, 'CHAPULTEPEC'), " +
                        "(5, 'SEVILLA'), (6, 'INSURGENTES'), (7, 'CUAUHTEMOC'), " +
                        "(8, 'BALDERAS '), (9, 'SALTO DEL AGUA'), " +
                        "(10, 'ISABEL LA CATOLICA'), (11, 'PINO SUAREZ'), (12, 'MERCED'), " +
                        "(13, 'CANDELARIA'), (14, 'SAN LAZARO'), (15, 'MOCTEZUMA'), " +
                        "(16, 'BALBUENA'), (17, 'BOULEVARD PUERTO AEREO'), " +
                        "(18, 'GOMEZ FARIAS'), (19, 'ZARAGOZA'), (20, 'PANTITLAN'), " +
                        "(21, 'CUATRO CAMINOS'), (22, 'PANTEONES'), (23, 'TACUBA'), " +
                        "(24, 'CUITLAHUAC'), (25, 'POPOTLA'), (26, 'COLEGIO MILITAR'), " +
                        "(27, 'NORMAL'), (28, 'SAN COSME'), (29, 'REVOLUCION'), " +
                        "(30, 'HIDALGO'), (31, 'BELLAS ARTES'), (32, 'ALLENDE'), " +
                        "(33, 'ZOCALO'), (34, 'SAN ANTONIO ABAD'), (35, 'CHABACANO'), " +
                        "(36, 'VIADUCTO'), (37, 'XOLA'), (38, 'VILLA DE CORTES'), " +
                        "(39, 'NATIVITAS'), (40, 'PORTALES'), (41, 'ERMITA'), " +
                        "(42, 'GENERAL ANAYA'), (43, 'TASQUENA'), (44, 'INDIOS VERDES'), " +
                        "(45, 'DEPORTIVO 18 DE MARZO'), (46, 'POTRERO'), (47, 'LA RAZA'), " +
                        "(48, 'TLATELOLCO'), (49, 'GUERRERO'), (50, 'JUAREZ'), " +
                        "(51, 'NINOS HEROES'), (52, 'HOSPITAL GENERAL'), " +
                        "(53, 'CENTRO MEDICO'), (54, 'ETIOPIA'), (55, 'EUGENIA'), " +
                        "(56, 'DIVISION DEL NORTE'), (57, 'ZAPATA'), (58, 'COYOACAN'), " +
                        "(59, 'VIVEROS'), (60, 'MIGUEL ANGEL DE QUEVEDO'), (61, 'COPILCO'), " +
                        "(62, 'UNIVERSIDAD'), (63, 'MARTIN CARRERA'), (64, 'TALISMAN'), " +
                        "(65, 'BONDOJITO'), (66, 'CONSULADO'), (67, 'CANAL DEL NORTE'), " +
                        "(68, 'MORELOS'), (69, 'FRAY SERVANDO'), (70, 'JAMAICA'), " +
                        "(71, 'SANTA ANITA'), (72, 'POLITECNICO'), " +
                        "(73, 'INSTITUTO DEL PETROLEO'), (74, 'AUTOBUSES DEL NORTE'), " +
                        "(75, 'MISTERIOS'), (76, 'VALLE GOMEZ'), (77, 'EDUARDO MOLINA'), " +
                        "(78, 'ARAGON'), (79, 'OCEANIA'), (80, 'TERMINAL AEREA'), " +
                        "(81, 'HANGARES'), (82, 'LA VILLA-BASILICA'), (83, 'LINDAVISTA'), " +
                        "(84, 'VALLEJO'), (85, 'NORTE 45'), (86, 'FERRERIA'), " +
                        "(87, 'AZCAPOTZALCO'), (88, 'TEZOZOMOC'), (89, 'EL ROSARIO'), " +
                        "(90, 'AQUILES SERDAN'), (91, 'CAMARONES'), (92, 'REFINERIA'), " +
                        "(93, 'SAN JOAQUIN'), (94, 'POLANCO'), (95, 'AUDITORIO'), " +
                        "(96, 'CONSTITUYENTES'), (97, 'SAN PEDRO DE LOS PINOS'), " +
                        "(98, 'SAN ANTONIO'), (99, 'MIXCOAC'), (100, 'BARRANCA DEL MUERTO'), " +
                        "(101, 'GARIBALDI'), (102, 'SAN JUAN DE LETRAN'), (103, 'DOCTORES'), " +
                        "(104, 'OBRERA'), (105, 'LA VIGA'), (106, 'COYUYA'), " +
                        "(107, 'IZTACALCO'), (108, 'APATLACO'), (109, 'ACULCO'), " +
                        "(110, 'ESCUADRON 201'), (111, 'ATLALILCO'), (112, 'IZTAPALAPA'), " +
                        "(113, 'CERRO DE LA ESTRELLA'), (114, 'UAM-I'), " +
                        "(115, 'CONSTITUCION DE 1917'), (116, 'PATRIOTISMO'), " +
                        "(117, 'CHILPANCINGO'), (118, 'LAZARO CARDENAS'), " +
                        "(119, 'MIXIUHCA'), (120, 'VELODROMO'), (121, 'CIUDAD DEPORTIVA'), " +
                        "(122, 'PUEBLA'), (123, 'AGRICOLA ORIENTAL'), " +
                        "(124, 'CANAL DE SAN JUAN'), (125, 'TEPALCATES'), " +
                        "(126, 'GUELATAO'), (127, 'PEÑON VIEJO'), " +
                        "(128, 'ACATITLA'), (129, 'SANTA MARTA'), (130, 'LOS REYES'), " +
                        "(131, 'LA PAZ'), (132, 'CIUDAD AZTECA'), (133, 'PLAZA ARAGON'), " +
                        "(134, 'OLIMPICA'), (135, 'ECATEPEC'), (136, 'MUZQUIZ'), " +
                        "(137, 'RIO DE LOS REMEDIOS'), (138, 'IMPULSORA'), " +
                        "(139, 'NEZAHUALCOYOTL'), (140, 'VILLA DE ARAGON'), " +
                        "(141, 'BOSQUE DE ARAGON'), (142, 'DEPORTIVO OCEANIA'), " +
                        "(143, 'ROMERO RUBIO'), (144, 'R. FLORES MAGON'), " +
                        "(145, 'TEPITO'), (146, 'LAGUNILLA'), (147, 'BUENAVISTA'), " +
                        "(148, 'GARAJE CUATRO CAMINOS'), (149, 'GARAJE PANTITLAN L5'), " +
                        "(150, 'GARAJE PANTITLAN L9'), (151, 'GARAJE PANTITLAN LA'), " +
                        "(152, 'TALLERES CIUDAD AZTECA'), (153, 'TALLERES CONSTITUCION'), " +
                        "(154, 'TALLERES EL ROSARIO'), (155, 'TALLERES LA PAZ'), " +
                        "(156, 'TALLERES TASQUENA'), (157, 'TALLERES TICOMAN'), " +
                        "(158, 'TALLERES ZARAGOZA'), (159, 'TAPON INDIOS VERDES'), " +
                        "(160, 'TAPON MARTIN CARRERA'), (161, 'TEPITO LB SURPONIENTE'), " +
                        "(162, 'TEPITO LB SURORIENTE'), (163, 'LAGUNILLA LB PTE'), " +
                        "(164, 'VILLA DE ARAGON LB'), (165, 'GARIBALDI L8'), " +
                        "(166, 'ZOCALO L2 LIBROS'), (167, 'ZOCALO L2 CATEDRAL'), " +
                        "(168, 'PINO SUAREZ L1'), (169, 'ZARAGOZA L1'), " +
                        "(170, 'BALDERAS L3'), (171, 'BALDERAS L1'), (172, 'HIDALGO L3'), " +
                        "(173, 'HIDALGO L2'), (174, 'TACUBA L2'), (175, 'TACUBA L7'), " +
                        "(176, 'TACUBA L7 CORRESPONDENCIA'), (177, 'TACUBAYA L1'), " +
                        "(178, 'TACUBAYA L7'), (179, 'TACUBAYA L9 PONIENTE'), " +
                        "(180, 'TACUBAYA L9 ORIENTE'), (181, 'PANTITLAN L9'), " +
                        "(182, 'CUATRO CAMINOS L2 SUR'), (183, 'CUATRO CAMINOS L2 NORTE'), " +
                        "(184, 'CHABACANO L9 SUR'), (185, 'CHABACANO L9 NORTE'), " +
                        "(186, 'SAN LAZARO L1'), (187, 'BELLAS ARTES L8'), " +
                        "(188, 'BELLAS ARTES L2'), (189, 'GUERRERO L3 OTE'), " +
                        "(190, 'GUERRERO L3 PTE'), (191, 'EDIFICIO PCC I'), " +
                        "(192, 'EDIFICIO PCC II'), (193, 'CENDI'), " +
                        "(194, 'EDIFICIO ADMINISTRATIVO'), (195, 'PERIMETRO DELICIAS'), " +
                        "(196, 'SEAT OCEANIA'), (197, 'SEAT ESTRELLA'), (198, 'INCADE'), " +
                        "(199, 'Otros'), (200, 'General'), (201, 'INSURGENTES SUR'), " +
                        "(202, 'HOSPITAL 20 DE NOVIEMBRE'), (203, 'PARQUE DE LOS VENADOS'), " +
                        "(204, 'EJE CENTRAL'), (205, 'MEXICALTZINGO'), (206, 'CULHUACAN'), " +
                        "(207, 'SAN ANDRES TOMATLAN'), (208, 'LOMAS ESTRELLA'), " +
                        "(209, 'CALLE 11'), (210, 'PERIFERICO OTE.'), (211, 'TEZONCO'), " +
                        "(212, 'OLIVOS'), (213, 'NOPALERA'), (214, 'ZAPOTITLAN'), " +
                        "(215, 'TLALTENCO'), (216, 'TLAHUAC'), " +
                        "(217, 'SIN DATOS O NO SE ENCUENTRA EN EL CATALOGO');");
                }
            }

            if (_connection.Table<TipoIncidencia>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO TipoIncidencia " +
                        "(IdTipoIncidencia,NombreTipoIncidencia) VALUES " +
                        "(1,'Abuso sexual'), (2, 'Arrollado'), (3, 'Atencion medica'), " +
                        "(4, 'Robo'), (5, 'Afectaciones al servicio'), (6, 'Extorcion'), " +
                        "(7, 'Fraude'), (8, 'Persona en zona de vias'), " +
                        "(9, 'Objeto en zona de vias'), (10, 'Faltas administrativas');");
                }
            }

            if (_connection.Table<Usuario>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO Usuario " +
                        "(NumEmpleado,Rfc,RolUsuario,NombreUsuario,ApellidoPaterno," +
                        "ApellidoMaterno,Password) VALUES " +
                        "(123456,'DEFR881023FGTY',1,'Juan','Lopez','Alvarado','8765432');");
                }
            }

            if (_connection.Table<SubtipoIncidencia>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO SubtipoIncidencia " +
                        "(IdSubtipoIncidencia ,NombreSubIncidencia,IdRelTipoIncidencia) " +
                        "VALUES (1,'Alumbramiento',3), (2, 'Enfermedad', 3), " +
                        "(3, 'Lesiones', 3), (4, 'Hostigamiento sexual', 1), " +
                        "(5, 'Inundaciones', 5), (6, 'Corte de corriente', 5), " +
                        "(7, 'Conato de incendio', 5), (8, 'Manifestacion', 5), " +
                        "(9, 'Porras', 5), (10, 'Ataques a las vias de comunicacion', 5), " +
                        "(11, 'Comercio informal', 10), (12, 'Grafitti', 10), " +
                        "(13, 'Ingeriendo bebidas alcoholicas', 10), " +
                        "(14, 'Ingeriendo o inalando sustancias toxicas', 10); ");
                }
            }

            if (_connection.Table<Roles>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO Roles (IdRol,NombreRol) " +
                        "VALUES (1,'Policia Auxiliar'), (2, 'Policia Bancario'), " +
                        "(3, 'Vigilancia Interna'); ");
                }
            }

            if (_connection.Table<CatalogoLinea>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO CatalogoLinea " +
                        "(IdLinea,NombreLinea,NombreZona) VALUES (1,'Linea 1','A'), " +
                        "(2, 'Linea 2', 'B'), (3, 'Linea 3', 'D'), (4, 'Linea 4', 'B'), " +
                        "(5, 'Linea 5', 'D'), (6, 'Linea 6', 'D'), (7, 'Linea 7', 'A'), " +
                        "(8, 'Linea 8', 'C'), (9, 'Linea 9', 'B'), (10, 'Linea A', 'A'), " +
                        "(11, 'Linea B', 'C'), (12, 'Linea 12', 'C'), (13, 'Taller', NULL), " +
                        "(14, 'Arcos', NULL), (15, 'Edificios', NULL), (16, 'Otros', NULL), " +
                        "(17, 'General', NULL), " +
                        "(18, 'SIN DATOS O NO SE ENCUENTRA EN EL CATALOGO', NULL);");
                }
            }                        

            if (_connection.Table<RelLineaEstacion>().Count() == 0)
            {
                lock (locker)
                {
                    _connection.Execute("INSERT INTO RelLineaEstacion " +
                        "(IdLineaEstacion,IdRelLinea,IdRelEstacion) VALUES (1,1,1), " +
                        "(2, 1, 2), (3, 1, 3), (4, 1, 4), (5, 1, 5), (6, 1, 6), " +
                        "(7, 1, 7), (8, 1, 8), (9, 1, 9), (10, 1, 10), (11, 1, 11), " +
                        "(12, 1, 12), (13, 1, 13), (14, 1, 14), (15, 1, 15), " +
                        "(16, 1, 16), (17, 1, 17), (18, 1, 18), (19, 1, 19), " +
                        "(20, 1, 20), (21, 2, 21), (22, 2, 22), (23, 2, 23), " +
                        "(24, 2, 24), (25, 2, 25), (26, 2, 26), (27, 2, 27), " +
                        "(28, 2, 28), (29, 2, 29), (30, 2, 30), (31, 2, 31), " +
                        "(32, 2, 32), (33, 2, 33), (34, 2, 11), (35, 2, 34), " +
                        "(36, 2, 35), (37, 2, 36), (38, 2, 37), (39, 2, 38), " +
                        "(40, 2, 39), (41, 2, 40), (42, 2, 41), (43, 2, 42), " +
                        "(44, 2, 43), (45, 3, 44), (46, 3, 45), (47, 3, 46), " +
                        "(48, 3, 47), (49, 3, 48), (50, 3, 49), (51, 3, 30), " +
                        "(52, 3, 50), (53, 3, 8), (54, 3, 51), (55, 3, 52), " +
                        "(56, 3, 53), (57, 3, 54), (58, 3, 55), (59, 3, 56), " +
                        "(60, 3, 57), (61, 3, 58), (62, 3, 59), (63, 3, 60), " +
                        "(64, 3, 61), (65, 3, 62), (66, 4, 63), (67, 4, 64), " +
                        "(68, 4, 65), (69, 4, 66), (70, 4, 67), (71, 4, 68), " +
                        "(72, 4, 13), (73, 4, 69), (74, 4, 70), (75, 4, 71), " +
                        "(76, 5, 72), (77, 5, 73), (78, 5, 74), (79, 5, 47), " +
                        "(80, 5, 75), (81, 5, 76), (82, 5, 66), (83, 5, 77), " +
                        "(84, 5, 78), (85, 5, 79), (86, 5, 80), (87, 5, 81), " +
                        "(88, 5, 20), (89, 6, 63), (90, 6, 82), (91, 6, 45), " +
                        "(92, 6, 83), (93, 6, 73), (94, 6, 84), (95, 6, 85), " +
                        "(96, 6, 86), (97, 6, 87), (98, 6, 88), (99, 6, 89), " +
                        "(100, 7, 89), (101, 7, 90), (102, 7, 91), (103, 7, 92), " +
                        "(104, 7, 23), (105, 7, 93), (106, 7, 94), (107, 7, 95), " +
                        "(108, 7, 96), (109, 7, 2), (110, 7, 97), (111, 7, 98), " +
                        "(112, 7, 99), (113, 7, 100), (114, 8, 101), (115, 8, 31), " +
                        "(116, 8, 102), (117, 8, 9), (118, 8, 103), (119, 8, 104), " +
                        "(120, 8, 35), (121, 8, 105), (122, 8, 71), (123, 8, 106), " +
                        "(124, 8, 107), (125, 8, 108), (126, 8, 109), (127, 8, 110), " +
                        "(128, 8, 111), (129, 8, 112), (130, 8, 113), (131, 8, 114), " +
                        "(132, 8, 115), (133, 9, 2), (134, 9, 116), (135, 9, 117), " +
                        "(136, 9, 53), (137, 9, 118), (138, 9, 35), (139, 9, 70), " +
                        "(140, 9, 119), (141, 9, 120), (142, 9, 121), (143, 9, 122), " +
                        "(144, 9, 20), (145, 10, 20), (146, 10, 123), (147, 10, 124), " +
                        "(148, 10, 125), (149, 10, 126), (150, 10, 127), " +
                        "(151, 10, 128), (152, 10, 129), (153, 10, 130), " +
                        "(154, 10, 131), (155, 11, 132), (156, 11, 133), " +
                        "(157, 11, 134), (158, 11, 135), (159, 11, 136), " +
                        "(160, 11, 137), (161, 11, 138), (162, 11, 139), " +
                        "(163, 11, 140), (164, 11, 141), (165, 11, 142), " +
                        "(166, 11, 79), (167, 11, 143), (168, 11, 144), " +
                        "(169, 11, 14), (170, 11, 68), (171, 11, 145), " +
                        "(172, 11, 146), (173, 11, 101), (174, 11, 49), " +
                        "(175, 11, 147), (176, 13, 148), (177, 13, 149), " +
                        "(178, 13, 150), (179, 13, 151), (180, 13, 152), " +
                        "(181, 13, 153), (182, 13, 154), (183, 13, 155), " +
                        "(184, 13, 156), (185, 13, 157), (186, 13, 158), " +
                        "(187, 13, 159), (188, 13, 160), (189, 14, 161), " +
                        "(190, 14, 162), (191, 14, 163), (192, 14, 164), " +
                        "(193, 14, 165), (194, 14, 166), (195, 14, 167), " +
                        "(196, 14, 168), (197, 14, 169), (198, 14, 170), " +
                        "(199, 14, 171), (200, 14, 172), (201, 14, 173), " +
                        "(202, 14, 174), (203, 14, 175), (204, 14, 176), " +
                        "(205, 14, 177), (206, 14, 178), (207, 14, 179), " +
                        "(208, 14, 180), (209, 14, 181), (210, 14, 182), " +
                        "(211, 14, 183), (212, 14, 184), (213, 14, 185), " +
                        "(214, 14, 186), (215, 14, 187), (216, 14, 188), " +
                        "(217, 14, 189), (219, 14, 190), (220, 15, 191), " +
                        "(221, 15, 192), (222, 15, 193), (223, 15, 194), " +
                        "(224, 15, 195), (225, 15, 196), (226, 15, 197), " +
                        "(227, 15, 198), (228, 16, 199), (229, 17, 200), " +
                        "(242, 12, 99), (243, 12, 201), (244, 12, 202), " +
                        "(245, 12, 57), (246, 12, 203), (247, 12, 204), " +
                        "(248, 12, 41), (249, 12, 205), (250, 12, 111), " +
                        "(251, 12, 206), (252, 12, 207), (253, 12, 208), " +
                        "(254, 12, 209), (255, 12, 210), (256, 12, 211), " +
                        "(257, 12, 212), (258, 12, 213), (259, 12, 214), " +
                        "(260, 12, 215), (261, 12, 216), (262, 1, 200), " +
                        "(263, 2, 200), (264, 3, 200), (265, 4, 200), " +
                        "(266, 5, 200), (267, 6, 200), (268, 7, 200), " +
                        "(269, 8, 200), (270, 9, 200), (271, 10, 200), " +
                        "(272, 11, 200), (273, 12, 200);");
                }
            }
        }

        public List<TipoIncidencia> GetTipoIncidencia()
        {            
            return _connection.Table<TipoIncidencia>().ToList();
        }

        public List<CatalogoLinea> GetLineas()
        {
            return _connection.Table<CatalogoLinea>().ToList();
        }

        public Usuario GetUsuario(string user, string pass)
        {
            return _connection.Table<Usuario>().Where(
                x=> x.NumEmpleado == user && x.Password == pass).Single();
        }
    }
}
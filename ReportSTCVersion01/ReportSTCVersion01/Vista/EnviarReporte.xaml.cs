﻿using Newtonsoft.Json;
using ReportSTCVersion01.Modelo;
using ReportSTCVersion01.ViewModel;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReportSTCVersion01.VistaModelo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnviarReporte : ContentPage
    {
        private string myDate;
        private Database db;
        //public string RecipeNameLabel;        
        private string _idEmpleado;
        private int _idLinea;
        private int _idEstacion;
        private int _idTipo;
        private string _myHour;
        //private readonly HttpClient client = new HttpClient();

        public EnviarReporte(string numEmpleado)
        {
            _myHour = DateTime.Today.ToString();
            myDate = DateTime.Today.ToString("yyyy-MM-dd");
            _idEmpleado = numEmpleado;
            //this.BindingContext = new MyViewModel(numEmpleado.ToString());            
            db = new Database();
            InitializeComponent();
            BindingContext = new ReporteViewModel();            
        }

        private void LineaPicker_OnSelectedIndexChanged(
            object sender, EventArgs args)
        {            
            var nameLinea = LineaTypePicker.Items[LineaTypePicker.SelectedIndex];
            //DisplayAlert("Linea seleccionada",nameLinea.ToString(),"OK");
            _idLinea = db.GetIdLinea(nameLinea.ToString());
            List<CatalogoEstacion> le = new List<CatalogoEstacion>();
            le = db.GetEstaciones(nameLinea.ToString());
            EstacionPicker.ItemsSource = le;            
        }

        private void EstacionPicker_OnSelectedIndexChanged(
            object sender, EventArgs args)
        {
            var name = EstacionPicker.Items[EstacionPicker.SelectedIndex];
            _idEstacion = db.GetIdEstacion(name.ToString());
            //DisplayAlert(name.ToString(), "Estacion", "OK");            
        }

        protected override void OnAppearing()
        {            
            base.OnAppearing();
            
            //await this.DisplayAlert("Datos", "" + cl.IdLinea, "Aceptar");
        }

        private void SubTypePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var subtipo = SubTypePicker.Items[SubTypePicker.SelectedIndex];
            //DisplayAlert(subtipo.ToString(), "Estacion", "OK");
        }

        private void ReporteTypePicker1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var nameTipo = TipoIncidentePicker.Items[TipoIncidentePicker.SelectedIndex];
            _idTipo = db.GetIdIncidencia(nameTipo.ToString());
            //DisplayAlert("Tipo de reporte seleccionado","Tipo: "+nameTipo.ToString()+
              //  "\nid:"+ _idTipo, "OK");          
            if (_idTipo.Equals(1) || _idTipo.Equals(3) || _idTipo.Equals(5) || _idTipo.Equals(10))
            {
                List<SubtipoIncidencia> le = new List<SubtipoIncidencia>();
                le = db.GetSubtipo(_idTipo);
                SubTypePicker.ItemsSource = le;
            }            
        }

        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            
            Reporte reporte = new Reporte();
            reporte.usuario_num_empleado = _idEmpleado;
            reporte.tipo_incidencia = _idTipo.ToString();
            reporte.fecha = myDate;
            reporte.hora = _myHour;
            reporte.foto_id_foto = "1";
            reporte.video_id_video = "1";
            reporte.audio_id_audio = "1";                                    
            var descripcion = editor.Text;// .ToString();
            if (string.IsNullOrEmpty(descripcion))
            {
                descripcion = "vacio";
            }
            reporte.observaciones = descripcion;
            reporte.rel_estacion_linea_id_relacion =
                db.GetIdLineaEstacion(_idLinea, _idEstacion).ToString();

            string RestUrl = "https://sistema-de-reportes-stc.000webhostapp.com/listado.php";
            var uri = new Uri(string.Format(RestUrl, string.Empty));

            var json = JsonConvert.SerializeObject(reporte);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            //DisplayAlert("Valor editor",descripcion,"Aceptar");

            /*
            //Uri geturi = new Uri("https://ws01.cenace.gob.mx:8082/SWPML/SIM/"
            //+ _sistema + "/" + _proceso + "/" + _nodo.Clave + "/" + _fecha + "/" + _fecha + "/xml");
            //HttpClient client = new HttpClient();
            //HttpResponseMessage responseGet = await client.GetAsync(geturi);
            //string response = await responseGet.Content.ReadAsStringAsync();                

            bool isNewItem = false;
            HttpResponseMessage response = null;
            if (isNewItem) {
                response = await client.PostAsync (uri, content);
            }

            if (response.IsSuccessStatusCode) {
                Debug.WriteLine (@"                TodoItem successfully saved.");
            }
             * */
            DisplayAlert("Objeto to json", "JSON: "+json+"\nUri:"+uri, "Aceptar");
        }
    }
}

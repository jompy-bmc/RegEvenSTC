﻿using ReportSTCVersion01.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ReportSTCVersion01.VistaModelo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Perfil : ContentPage
    {
        private LoginPageViewModel viewModel;
        public Perfil()
        {
            InitializeComponent();
            BindingContext = viewModel = new LoginPageViewModel();
            viewModel.Navigation = this.Navigation;
        }
 
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}

﻿using ReportSTCVersion01.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportSTCVersion01.ViewModel
{
    public class EstacionesViewModel : BaseViewModel
    {
        private string _id;
        public List<CatalogoEstacion> ListEstaciones
        {
            get;
            set;
        }
    
       
        public EstacionesViewModel(string id)
        {
            this._id = id;

           ListEstaciones = PickerServiceEstacion.GetEstaciones(_id).OrderBy(c => c.NombreEstacion).ToList();
        }
           
    }
}

﻿using ReportSTCVersion01.Modelo;
using System.Collections.Generic;

namespace ReportSTCVersion01.Service
{
    public class PickerService
    {
        public static List<TipoIncidencia> GetCities()
        {
            Database db = new Database();
            var cities = new List<TipoIncidencia>();
            cities = db.GetTipoIncidencia();
            return cities;
        }
    }
}
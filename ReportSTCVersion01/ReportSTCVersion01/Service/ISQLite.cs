﻿using SQLite.Net;

namespace ReportSTCVersion01.Service
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}

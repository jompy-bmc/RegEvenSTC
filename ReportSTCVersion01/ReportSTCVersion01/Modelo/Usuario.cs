﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("Usuario")]
    public class Usuario
    {        
        [Column("NumEmpleado")]
        public string NumEmpleado { get; set; }

        [PrimaryKey]
        [Column("Rfc")]
        public string Rfc { get; set; }

        [Column("RolUsuario")]
        public int RolUsuario { get; set; }

        [Column("NombreUsuario")]
        public string NombreUsuario { get; set; }

        [Column("ApellidoPaterno")]
        public string ApellidoPaterno { get; set; }

        [Column("ApellidoMaterno")]
        public string ApellidoMaterno { get; set; }

        [Column("Password")]
        public string Password { get; set; }
    }
}

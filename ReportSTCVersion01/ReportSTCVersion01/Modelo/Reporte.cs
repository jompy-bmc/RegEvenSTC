﻿using SQLite.Net.Attributes;
using System;

namespace ReportSTCVersion01.Modelo
{
    [Table("Reporte")]
    public class Reporte
    {
        [PrimaryKey]
        [Column("usuario_num_empleado")]
        public string usuario_num_empleado { get; set; }        

        [Column("tipo_incidencia")]
        public string tipo_incidencia { get; set; }

        [Column("fecha")]
        public string fecha { get; set; }

        [Column("hora")]
        public string hora { get; set; }

        //[Column("IdSubtipoIncidencia")]
        //public int IdSubtipoIncidencia { get; set; }

        [Column("foto_id_foto")]
        public string foto_id_foto { get; set; }

        //[Column("FormatoFoto")]
        //public string FormatoFoto { get; set; }

        [Column("video_id_video")]
        public string video_id_video { get; set; }

        //[Column("FormatoVideo")]
        //public string FormatoVideo { get; set; }

        [Column("audio_id_audio")]
        public string audio_id_audio { get; set; }

        //[Column("FormatoAudio")]
        //public string FormatoAudio { get; set; }       

        [Column("observaciones")]
        public string observaciones { get; set; }

        [Column("rel_estacion_linea_id_relacion")]
        public string rel_estacion_linea_id_relacion { get; set; }
    }
}
﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("Roles")]
    public class Roles
    {
        [PrimaryKey]
        [Column("IdRol")]
        public int IdRol { get; set; }

        [Column("NombreRol")]
        public string NombreRol { get; set; }
    }
}
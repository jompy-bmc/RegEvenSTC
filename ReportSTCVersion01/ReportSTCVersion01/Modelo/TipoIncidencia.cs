﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("TipoIncidencia")]
    public class TipoIncidencia
    {
        [PrimaryKey]
        [Column("IdTipoIncidencia")]
        public int IdTipoIncidencia { get; set; }

        [Column("NombreTipoIncidencia")]
        public string NombreTipoIncidencia { get; set; }
    }
}
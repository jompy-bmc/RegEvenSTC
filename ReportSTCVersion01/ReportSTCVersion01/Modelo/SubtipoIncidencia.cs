﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("SubtipoIncidencia")]
    public class SubtipoIncidencia
    {
        [PrimaryKey]
        [Column("IdSubtipoIncidencia")]
        public int IdSubtipoIncidencia { get; set; }

        [Column("NombreSubIncidencia")]
        public string NombreSubIncidencia { get; set; }

        [Column("IdRelTipoIncidencia")]
        public int IdRelTipoIncidencia { get; set; }
    }
}
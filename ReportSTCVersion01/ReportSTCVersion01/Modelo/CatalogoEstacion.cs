﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("CatalogoEstacion")]
    public class CatalogoEstacion
    {
        [PrimaryKey]
        [Column("IdEstacion")]
        public int IdEstacion { get; set; }

        [Column("NombreEstacion")]
        public string NombreEstacion { get; set; }
    }
}